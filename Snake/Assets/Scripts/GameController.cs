﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameController : MonoBehaviour {

    public int maxSize;
    public int currentSize;
    public int xBound;
    public int yBound;
    public int score;
    public GameObject foodPrefab;
    public GameObject currentFood;
    public GameObject snakePrefab;
    public Snake head;
    public Snake tail;
    public int NESW;
    public Vector2 nexPos;
    public Text scoreText;
    public float deltaTimer;

    void OnEnable()
    {
        Snake.hit += hit;
    }

    void OnDisable()
    {
        Snake.hit -= hit;
    }
    // Use this for initialization
    void Start () {

        InvokeRepeating("timerInvoke", 0, deltaTimer);
        foodFunction();
	}
	
	// Update is called once per frame
	void Update () {
        comChangeD();
	}

    void timerInvoke()
    {
        movement();
        StartCoroutine(checkVisible());
        if (currentSize >= maxSize)
        {
            tailFunction();
        }else
        {
            currentSize++;
        }
    }

    void movement()
    {
        GameObject temp;
        nexPos = head.transform.position;
        switch (NESW)
        {
            case 0:
                nexPos = new Vector2(nexPos.x, nexPos.y + 1);
                break;
            case 1:
                nexPos = new Vector2(nexPos.x + 1, nexPos.y);
                break;
            case 2:
                nexPos = new Vector2(nexPos.x, nexPos.y - 1);
                break;
            case 3:
                nexPos = new Vector2(nexPos.x - 1, nexPos.y);
                break;
        }
        temp = (GameObject)Instantiate(snakePrefab,nexPos,transform.rotation);
        head.setNext(temp.GetComponent<Snake>());
        head = temp.GetComponent<Snake>();

        return;
    }

    void comChangeD()
    {
        if (NESW != 2 && Input.GetKeyDown(KeyCode.W))
        {
            NESW = 0;
        }
        if (NESW != 3 && Input.GetKeyDown(KeyCode.D))
        {
            NESW = 1;
        }
        if (NESW != 0 && Input.GetKeyDown(KeyCode.S))
        {
            NESW = 2;
        }
        if (NESW != 1 && Input.GetKeyDown(KeyCode.A))
        {
            NESW = 3;
        }

    }

    void tailFunction()
    {
        Snake tempSnake = tail;
        tail = tail.getNext();
        tempSnake.removeTail();
    }

    void foodFunction()
    {
        int xPos = Random.Range(-xBound, xBound);
        int yPos = Random.Range(-yBound, yBound);

        currentFood = (GameObject)Instantiate(foodPrefab, new Vector2(xPos, yPos), transform.rotation);
        StartCoroutine(checkRender(currentFood));
    }

    IEnumerator checkRender(GameObject IN)
    {
        yield return new WaitForEndOfFrame();
        if (IN.GetComponent<Renderer>().isVisible==false)
        {
            if (IN.tag == "Food")
            {
                Destroy(IN);
                foodFunction();
            }
        }
    }

    void hit(string WhatWasSend)
    {
        if(WhatWasSend == "Food")
        {
            if(deltaTimer >= .1f)
            {
                deltaTimer -= .05f;
                CancelInvoke("timerInvoke");
                InvokeRepeating("timerInvoke", 0, deltaTimer);
            }
            
            foodFunction();
            maxSize++;
            score++;
            scoreText.text = score.ToString();
            int temp = PlayerPrefs.GetInt("HighScore");
            if (score > temp)
            {
                PlayerPrefs.SetInt("HighScore", score);
            }
        }
        if(WhatWasSend == "Snake")
        {
            CancelInvoke("timerInvoke");
            exit();
        }
    }

    public void exit()
    {
        SceneManager.LoadScene(0);
    }

    void wrap()
    {
        if(NESW == 0)
        {
            head.transform.position = new Vector2(head.transform.position.x, (head.transform.position.y - 46));
        }
        else if (NESW == 1)
        {
            head.transform.position = new Vector2(-(head.transform.position.x - 1 ), head.transform.position.y);
        }
        else if (NESW == 2)
        {
            head.transform.position = new Vector2(head.transform.position.x, (head.transform.position.y - 4));
        }
        else if (NESW == 3)
        {
            head.transform.position = new Vector2(-(head.transform.position.x + 1 ), head.transform.position.y);
        }
    }

    IEnumerator checkVisible()
    {
        yield return new WaitForEndOfFrame();
        if (!head.GetComponent<Renderer>().isVisible)
        {
            wrap();
        }
    }
}
