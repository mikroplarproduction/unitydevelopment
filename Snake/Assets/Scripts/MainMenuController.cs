﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    public Text hS;

	// Use this for initialization
	void Start () {
        hsFunction();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    void hsFunction()
    {
        hS.text = PlayerPrefs.GetInt("HighScore").ToString();
    }
}
 